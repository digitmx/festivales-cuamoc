<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		//Read Session
		$logged = (isset($_SESSION['fc_logged'])) ? $_SESSION['fc_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url() ); }
	}

	public function index()
	{
		//Consultamos los datos
		$query = $this->db->query("SELECT * FROM event WHERE status = 1 ORDER BY idevent DESC");
		$data['events'] = $query->result();
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('dashboard/index', $data);
		$this->load->view('includes/footer');		
	}
	
}