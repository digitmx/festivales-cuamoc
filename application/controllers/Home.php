<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		//Read Session
		$logged = (isset($_SESSION['fc_logged'])) ? $_SESSION['fc_logged'] : false;

		//Check Session
		if ($logged) { redirect( base_url().'dashboard' ); }
	}
	
	public function index()
	{
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('general/home');
		$this->load->view('includes/footer');
	}
}
