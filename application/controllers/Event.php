<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		//Read Session
		$logged = (isset($_SESSION['fc_logged'])) ? $_SESSION['fc_logged'] : false;

		//Check Session
		if (!$logged) { redirect( base_url() ); }
	}
	
	public function index()
	{
		//Consultamos los datos
		$query = $this->db->query("SELECT * FROM event WHERE status = 1 ORDER BY idevent DESC");
		$data['events'] = $query->result();
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('dashboard/index', $data);
		$this->load->view('includes/footer');
	}

	public function create()
	{
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('event/new');
		$this->load->view('includes/footer');		
	}
	
	public function edit()
	{
		//Leemos el id del evento
		$idevent = $this->uri->segment(3, 0);
		
		//Consultamos los datos
		$query = $this->db->query("SELECT * FROM event WHERE idevent = " . $idevent . " AND status = 1 LIMIT 1");
		$data['event'] = $query->row();
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('event/edit', $data);
		$this->load->view('includes/footer');
	}
	
	public function delete()
	{
		//Leemos el id del evento
		$idevent = $this->uri->segment(3, 0);
		
		//Guardamos los Datos
        $data = array(
	        'status' => 0
        );
        $this->db->where('idevent', $idevent);
        $this->db->update('event', $data);
        
        //Redirect a Dashboard
        redirect( base_url() . 'dashboard' );
	}
	
	public function saveEvent()
	{
		//Cargamos la imagen
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '20480';
		$config['file_name'] = now();
		$config['overwrite'] = FALSE;
		$config['remove_spaces'] = TRUE;

		//load upload class library
        $this->load->library('upload', $config);
        
        //variables de imagenes
        $upload_logo = ''; $upload_cartel = ''; $upload_fondo = '';

		//Logo
        if (!$this->upload->do_upload('inputLogo'))
        {
            // case - failure
            $upload_error_logo = array('error' => $this->upload->display_errors());
        }
        else
        {
            // case - success
            $upload_data = $this->upload->data();
            $upload_logo = base_url() . 'uploads/' . $upload_data['file_name'];
        }

		//load upload class library
        $this->load->library('upload', $config);
        
        //Cartel
        if (!$this->upload->do_upload('inputCartel'))
        {
            // case - failure
            $upload_error_cartel = array('error' => $this->upload->display_errors());
        }
        else
        {
            // case - success
            $upload_data = $this->upload->data();
            $upload_cartel = base_url() . 'uploads/' . $upload_data['file_name'];
        }

		//load upload class library
        $this->load->library('upload', $config);
        
        //Fondo
        if (!$this->upload->do_upload('inputFondo'))
        {
            // case - failure
            $upload_error_fondo = array('error' => $this->upload->display_errors());
        }
        else
        {
            // case - success
            $upload_data = $this->upload->data();
            $upload_fondo = base_url() . 'uploads/' . $upload_data['file_name'];
        }
        
        //Guardamos los Datos
        $data = array(
	        'name' => $_POST['inputNombre'],
	        'logo' => $upload_logo,
	        'background' => $upload_fondo,
	        'cartel' => $upload_cartel,
	        'cofepris' => $_POST['inputCofepris'],
	        'prize' => $_POST['inputPremio'],
	        'step1' => $_POST['inputPaso1'],
	        'step2' => $_POST['inputPaso2'],
	        'step3' => $_POST['inputPaso3'],
	        'description' => $_POST['inputInstrucciones'],
	        'message' => $_POST['inputMensaje'],
	        'codes' => $_POST['inputCodigos'],
	        'active' => 0,
	        'createdAt' => date('Y-m-d H:i:s'),
	        'status' => 1
        );
        $this->db->insert('event', $data);
        
        //Redirect a Dashboard
        redirect( base_url() . 'dashboard' );
	}
	
	public function updateEvent()
	{
		//Cargamos la imagen
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '20480';
		$config['file_name'] = now();
		$config['overwrite'] = FALSE;
		$config['remove_spaces'] = TRUE;

		//load upload class library
        $this->load->library('upload', $config);
        
        //variables de imagenes
        $upload_logo = $_POST['oldLogo']; $upload_cartel = $_POST['oldCartel']; $upload_fondo = $_POST['oldBackground'];

		//Logo
        if (!$this->upload->do_upload('inputLogo'))
        {
            // case - failure
            $upload_error_logo = array('error' => $this->upload->display_errors());
        }
        else
        {
            // case - success
            $upload_data = $this->upload->data();
            $upload_logo = base_url() . 'uploads/' . $upload_data['file_name'];
        }

		//load upload class library
        $this->load->library('upload', $config);
        
        //Cartel
        if (!$this->upload->do_upload('inputCartel'))
        {
            // case - failure
            $upload_error_cartel = array('error' => $this->upload->display_errors());
        }
        else
        {
            // case - success
            $upload_data = $this->upload->data();
            $upload_cartel = base_url() . 'uploads/' . $upload_data['file_name'];
        }

		//load upload class library
        $this->load->library('upload', $config);
        
        //Fondo
        if (!$this->upload->do_upload('inputFondo'))
        {
            // case - failure
            $upload_error_fondo = array('error' => $this->upload->display_errors());
        }
        else
        {
            // case - success
            $upload_data = $this->upload->data();
            $upload_fondo = base_url() . 'uploads/' . $upload_data['file_name'];
        }
        
        //Guardamos los Datos
        $data = array(
	        'name' => $_POST['inputNombre'],
	        'logo' => $upload_logo,
	        'background' => $upload_fondo,
	        'cartel' => $upload_cartel,
	        'cofepris' => $_POST['inputCofepris'],
	        'prize' => $_POST['inputPremio'],
	        'step1' => $_POST['inputPaso1'],
	        'step2' => $_POST['inputPaso2'],
	        'step3' => $_POST['inputPaso3'],
	        'description' => $_POST['inputInstrucciones'],
	        'message' => $_POST['inputMensaje'],
	        'codes' => $_POST['inputCodigos'],
	        'createdAt' => date('Y-m-d H:i:s'),
	        'status' => 1
        );
        $this->db->where('idevent', $_POST['idevent']);
        $this->db->update('event', $data);
        
        //Redirect a Dashboard
        redirect( base_url() . 'dashboard' );
	}
	
	public function users()
	{
		//Leemos el id del evento
		$idevent = $this->uri->segment(3, 0);
		
		//Consultamos los datos
		$query = $this->db->query("SELECT * FROM event WHERE idevent = " . $idevent . " AND status = 1 LIMIT 1");
		$data['event'] = $query->row();
		
		//Consultamos los usuarios
		$query = $this->db->query("SELECT (SELECT iduser FROM user where user.iduser = user_event.iduser) as iduser, (SELECT fb_id FROM user where user.iduser = user_event.iduser) as fb_id, (SELECT name FROM user where user.iduser = user_event.iduser) as name, (SELECT lastname FROM user where user.iduser = user_event.iduser) as lastname, (SELECT email FROM user where user.iduser = user_event.iduser) as email, (SELECT birthday FROM user where user.iduser = user_event.iduser) as birthday, user_event.code as code, user_event.status as redimido FROM user_event WHERE idevent = " . $idevent);
		$data['users'] = $query->result();
		
		//Consultamos los usuarios redimidos
		$query = $this->db->query("SELECT * FROM user_event WHERE idevent = " . $idevent . " AND status = 1");
		$data['users_used'] = $query->result();
		
		//Load Views
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('event/users', $data);
		$this->load->view('includes/footer');
	}
	
	public function export()
	{
		//Leemos el id del evento
		$idevent = $this->uri->segment(3, 0);
		
		//Consultamos los usuarios
		$query = $this->db->query("SELECT (SELECT iduser FROM user where user.iduser = user_event.iduser) as iduser, (SELECT fb_id FROM user where user.iduser = user_event.iduser) as fb_id, (SELECT name FROM user where user.iduser = user_event.iduser) as name, (SELECT lastname FROM user where user.iduser = user_event.iduser) as lastname, (SELECT email FROM user where user.iduser = user_event.iduser) as email, (SELECT birthday FROM user where user.iduser = user_event.iduser) as birthday, user_event.code as code, user_event.status as redimido FROM user_event WHERE idevent = " . $idevent);
		$users = array();
		foreach ($query->result() as $row)
		{
			$users[] = array(
				'iduser' => $row->iduser,
				'fb_id' => $row->fb_id,
				'name' => $row->name,
				'lastname' => $row->lastname,
				'email' => $row->email,
				'birthday' => $row->birthday,
				'code' => $row->code,
				'redimido' => $row->redimido
			);
		}
		
		header('Content-Encoding: UTF-8');
		header("Content-type: text/csv; charset=UTF-8");
		header("Content-Disposition: attachment; filename=report.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		echo "\xEF\xBB\xBF";
		$fp = fopen('php://output', 'w');
		foreach ($users as $fields) {
		    fputcsv($fp, $fields);
		}
		fclose($fp);
			
        exit;
	}
	
	public function activate()
	{
		//Leemos el id del evento
		$idevent = $this->uri->segment(3, 0);
		
		/*
		//Consultamos los datos
		$query = $this->db->query("SELECT * FROM event WHERE status = 1 ORDER BY idevent DESC");
		
		//Leemos todos los eventos
		foreach ($query->result() as $row)
		{
			//Actualizamos los Datos
	        $data = array(
		       	'active' => 0
	        );
	        $this->db->where('idevent', $row->idevent);
	        $this->db->update('event', $data);
		}*/
		
		//Actualizamos los Datos
        $data = array(
	       	'active' => 1
        );
        $this->db->where('idevent', $idevent);
        $this->db->update('event', $data);
        
        //Redirect a Dashboard
        redirect( base_url() . 'dashboard' );
	}
	
	public function inactivate()
	{
		//Leemos el id del evento
		$idevent = $this->uri->segment(3, 0);
		
		/*
		//Consultamos los datos
		$query = $this->db->query("SELECT * FROM event WHERE status = 1 ORDER BY idevent DESC");
		
		//Leemos todos los eventos
		foreach ($query->result() as $row)
		{
			//Actualizamos los Datos
	        $data = array(
		       	'active' => 0
	        );
	        $this->db->where('idevent', $row->idevent);
	        $this->db->update('event', $data);
		}*/
		
		//Actualizamos los Datos
        $data = array(
	       	'active' => 0
        );
        $this->db->where('idevent', $idevent);
        $this->db->update('event', $data);
        
        //Redirect a Dashboard
        redirect( base_url() . 'dashboard' );
	}
	
}