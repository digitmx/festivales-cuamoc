<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Functions extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function uploadFile($full_path,$name)
	{
		//Verificamos si existe la carpeta de uploads
		$this->load->library('ftp');

		$config['hostname'] = 'fooprojects.com';
		$config['username'] = 'festivales-cuamoc@fooprojects.com';
		$config['password'] = 'eh2B]{{mlH9';
		
		//Conectamos con el FTP
		$this->ftp->connect($config);
		
		// CARPETA AÑO
		//Buscamos la carpeta del año
		$anio = FALSE;
		$list = $this->ftp->list_files('/uploads/');
		
		//Verificamos si existe la carpeta de año
		foreach ($list as $row)
		{
			if ($row == '/uploads/'.date('Y'))
			{
				$anio = TRUE;
			}
		}
		
		//Verificamos si existe
		if (!$anio)
		{
			//Creamos el folder del año en Uploads
			$this->ftp->mkdir('/uploads/'.date('Y/'), DIR_WRITE_MODE);
		}
		
		// CARPETA MES
		//Buscamos la carpeta del mes
		$mes = FALSE;
		$list = $this->ftp->list_files('/uploads/'.date('Y/'));
		
		//Verificamos si existe la carpeta de mes
		foreach ($list as $row)
		{
			if ($row == '/uploads/'.date('Y/m'))
			{
				$mes = TRUE;
			}
		}
		
		//Verificamos si existe
		if (!$mes)
		{
			//Creamos el folder del mes en Uploads
			$this->ftp->mkdir('/uploads/'.date('Y/m/'), DIR_WRITE_MODE);
		}
		
		// CARPETA DIA
		//Buscamos la carpeta del dia
		$dia = FALSE;
		$list = $this->ftp->list_files('/uploads/'.date('Y/m/'));
		
		//Verificamos si existe la carpeta de dia
		foreach ($list as $row)
		{
			if ($row == '/uploads/'.date('Y/m/d'))
			{
				$dia = TRUE;
			}
		}
		
		//Verificamos si existe
		if (!$dia)
		{
			//Creamos el folder del dia en Uploads
			$this->ftp->mkdir('/uploads/'.date('Y/m/d'), DIR_WRITE_MODE);
		}
		
		//Subimos el archivo local al FTP Destino
		$this->ftp->upload($full_path, '/uploads/'.date('Y/m/d/').$name, 'auto', 0775);
		
		//Cerramos la conexión
		$this->ftp->close();
		
		//Borramos el archivo local
		unlink($full_path);
		
		//Regresamos la ruta completa del archivo
		return 'http://fooprojects.com/clients/festivales-cuamoc/uploads/'.date('Y/m/d/').$name;
	}

}
