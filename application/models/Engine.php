<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// How to send the response header with PHP
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Engine extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	//Funcion para identar JSON
	public function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	public function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo $this->indent($json);
	}

	//Manejador de Mensajes Recibidos en el JSON
	public function executeJSON($msg,$fields,$app,$apikey)
	{
		//Leemos la Configuracion
		$output = FALSE;
		
		// getEventID
		if ($msg == 'getEvent')
		{
			//Leemos los Datos
			$idevent = (isset($fields['idfestival'])) ? (string)trim($fields['idfestival']) : '';
			$query_event = $this->db->query("SELECT * FROM event WHERE idevent = ".$idevent." AND status = 1 AND active = 1 ORDER BY idevent DESC LIMIT 1");
			
			//Verificamos si hay datos
			if ($query_event->num_rows() > 0)
			{
				//Leemos el Objeto
				$row_object = $query_event->row();
				
				//Consultamos
				$data = array(
					'idfestival' => $row_object->idevent,
				    'nombre' => $row_object->name,
				    'logo' => $row_object->logo,
				    'imagen_fondo' => $row_object->background,
				    'cartel' => $row_object->cartel,
				    'cofepris' => $row_object->cofepris,
				    'premio' => $row_object->prize,
				    'dinamica' => array(
					    'paso1' => $row_object->step1,
					    'paso2' => $row_object->step2,
					    'paso3' => $row_object->step3,
				    ),
				    'instrucciones' => $row_object->description,
				    'mensaje_agotados' => $row_object->message,
				    'codigos' => $row_object->codes
				);
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $data
				);
					
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No existe un festival con ese ID.'
				);
	
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}

		// getEventID
		if ($msg == 'getEventID')
		{
			//Leemos los Datos
			$query_event = $this->db->query("SELECT * FROM event WHERE status = 1 AND active = 1 ORDER BY idevent DESC LIMIT 1");
			
			//Verificamos si hay datos
			if ($query_event->num_rows() > 0)
			{
				//Leemos el Objeto
				$row_object = $query_event->row();
				
				//Consultamos
				$data = array(
					'idfestival' => $row_object->idevent,
				    'nombre' => $row_object->name,
				    'logo' => $row_object->logo,
				    'imagen_fondo' => $row_object->background,
				    'cartel' => $row_object->cartel,
				    'cofepris' => $row_object->cofepris,
				    'premio' => $row_object->prize,
				    'dinamica' => array(
					    'paso1' => $row_object->step1,
					    'paso2' => $row_object->step2,
					    'paso3' => $row_object->step3,
				    ),
				    'instrucciones' => $row_object->description,
				    'mensaje_agotados' => $row_object->message,
				    'codigos' => $row_object->codes
				);
				
				//Generamos el Arreglo
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $data
				);
					
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'No hay festivales activos en este momento.'
				);
	
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// sendUserData
		if ($msg == 'sendUserData')
		{
			//Leemos los Datos
			$fb_id = (isset($fields['fb_id'])) ? (string)trim($fields['fb_id']) : '';
			$name = (isset($fields['nombre'])) ? (string)trim($fields['nombre']) : '';
			$lastname = (isset($fields['apellido'])) ? (string)trim($fields['apellido']) : '';
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$state = (isset($fields['estado'])) ? (string)trim($fields['estado']) : '';
			$birthday = (isset($fields['fecha_nacimiento'])) ? (string)trim($fields['fecha_nacimiento']) : '';
			$idevent = (isset($fields['idfestival'])) ? (string)trim($fields['idfestival']) : '';
			$code = (isset($fields['codigo'])) ? (string)trim($fields['codigo']) : '';
			
			//Verificamos que traiga los datos
			if ($fb_id && $name && $lastname && $email && $state && $birthday && $idevent && $code)
			{
				//Consultamos el Evento
				$query_event = $this->db->query("SELECT * FROM event WHERE idevent = '" . $idevent . "' ORDER BY idevent DESC LIMIT 1");
				
				//Verificamos si existe el evento y es válido
				if ($query_event->num_rows() > 0)
				{
					//Leemos el Objeto
					$row_event = $query_event->row();
						
					//Creamos el Arreglo
					$data = array(
						'fb_id' => $fb_id,
						'name' => $name,
						'lastname' => $lastname,
						'email' => $email,
						'state' => $state,
						'birthday' => $birthday,
						'status' => 1
					);
					
					//Leemos los Datos
					$query_user = $this->db->query("SELECT * FROM user WHERE fb_id = '" . $fb_id . "' ORDER BY iduser DESC LIMIT 1");
					
					//Verificamos si hay datos
					if ($query_user->num_rows() == 0)
					{
						//Registramos al Usuario
						$data['createdAt'] = date('Y-m-d H:i:s');
						$this->db->insert('user', $data);
						$iduser = $this->db->insert_id();
					}
					else
					{
						//Leemos el Objeto
						$row_user = $query_user->row();
						$iduser = $row_user->iduser;
						
						//Actualizamos los datos del usuario
						$data['updatedAt'] = date('Y-m-d H:i:s');
						$this->db->where('iduser', $iduser);
						$this->db->update('user', $data);
					}
					
					//Consultamos si el Usuario ya se registro al evento
					$query_user_event = $this->db->query("SELECT * FROM user_event WHERE iduser = " . $iduser . " AND idevent = " . $idevent . " LIMIT 1");
					
					//Verificamos si ya existe su registro en el evento
					if ($query_user_event->num_rows() == 0)
					{
						//Registramos su participación
						$data_user_event = array(
							'iduser' => $iduser,
							'idevent' => $idevent,
							'code' => $code,
							'createdAt' => date('Y-m-d H:i:s'),
							'status' => 0
						);
						$this->db->insert('user_event', $data_user_event);
						$iduser_event = $this->db->insert_id();
						
						//Generamos el Arreglo de Respuesta
						$response = array(
							'usuario' => array(
								'idusuario' => $iduser,
								'fb_id' => $fb_id,
								'nombre' => $name,
								'apellido' => $lastname,
								'email' => $email,
								'estado' => $state,
								'fecha_nacimiento' => $birthday
							),
							'festival' => array(
								'idfestival' => $row_event->idevent,
							    'nombre' => $row_event->name,
							    'logo' => $row_event->logo,
							    'imagen_fondo' => $row_event->background,
							    'cartel' => $row_event->cartel,
							    'cofepris' => $row_event->cofepris,
							    'premio' => $row_event->prize,
							    'dinamica' => array(
								    'paso1' => $row_event->step1,
								    'paso2' => $row_event->step2,
								    'paso3' => $row_event->step3,
							    ),
							    'instrucciones' => $row_event->description,
							    'mensaje_agotados' => $row_event->message,
							    'codigos' => $row_event->codes
							),
							'codigo' => $code
						);
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $response
						);
						
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'Este usuario ya está registrado para este festival.'
						);
			
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este festival no existe.'
					);
		
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);
	
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// verifyQR
		if ($msg == 'verifyQR')
		{
			//Leemos los datos
			$code = (isset($fields['codigo'])) ? (string)trim($fields['codigo']) : '';
			$idevent = (isset($fields['idfestival'])) ? (string)trim($fields['idfestival']) : '';
			
			//Verificamos los datos
			if ($code && $idevent)
			{
				//Consultamos el Evento y el Código
				/*$query_user_event = $this->db->query("SELECT * FROM user_event WHERE code = '" . $code . "' AND idevent = " . $idevent . " AND status = 0 LIMIT 1");*/
				$query_user_event = $this->db->query("SELECT * FROM user_event WHERE code = '" . $code . "' LIMIT 1");
				
				//Verificamos si ya existe su registro en el evento
				if ($query_user_event->num_rows() > 0)
				{
					//Leemos el Objeto User Event
					$row_user_event = $query_user_event->row();
					
					//Revisamos si ya se ocupo el cupón con el evento
					if ($row_user_event->status == 0)
					{
						//Actualizamos el Status del User Event
						$data = array(
							'updatedAt' => date('Y-m-d H:i:s'),
						    'status' => 1
						);
						$this->db->where('iduser_event', $row_user_event->iduser_event);
						$this->db->update('user_event', $data);
						
						//Consultamos el usuario
						$query_user = $this->db->query("SELECT * FROM user WHERE iduser = " . $row_user_event->iduser . " ORDER BY iduser DESC LIMIT 1");
						
						//Leemos el Objeto User
						$row_user = $query_user->row();
						
						//Generamos el Arreglo de Respuesta
						$response = array(
							'idusuario' => $row_user->iduser,
							'fb_id' => $row_user->fb_id,
							'nombre' => $row_user->name,
							'apellido' => $row_user->lastname,
							'email' => $row_user->email,
							'estado' => $row_user->state,
							'fecha_nacimiento' => $row_user->birthday
						);
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'El código ha sido canjeado con éxito.',
							'data' => $response
						);
						
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Mostrar Error
						$array = array(
							'status' => (int)2,
							'msg' => (string)'El código ha sido cobrado anteriormente. Ingresa un código nuevo.'
						);
			
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'El código ingresado no existe.'
					);
		
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)3,
					'msg' => (string)'Ha ocurrido un error, intenta de nuevo.'
				);
	
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		// loginAdmin
		if ($msg == 'loginAdmin')
		{
			//Leemos los datos
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			
			//Verificamos los datos
			if ($email && $password)
			{
				//Consultamos los datos
				$query = $this->db->query("SELECT * FROM admin WHERE email = '" . $email . "' AND password = '" . sha1($password) . "' AND status = 1 LIMIT 1");
				
				//Verificamos si existe el usuario
				if ($query->num_rows() > 0)
				{
					//Leemos el Objeto
					$row = $query->row();
					
					//Consultamos
					$data = array(
						'idadmin' => $row->idadmin,
					    'email' => $row->email,
					    'name' => $row->name
					);
					
					//Save Admin in $_SESSION
					$this->session->set_userdata('admin', $data);
					$this->session->set_userdata('fc_logged', true);
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success',
						'data' => $data
					);
						
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Mostrar Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Credenciales incorrectas. Intenta de nuevo.'
					);
		
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Mostrar Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.'
				);
	
				//Imprimimos el Arreglo
				$this->printJSON($array);
				$output = TRUE;
			}
		}
		
		if (!$output)
		{
			//Mostrar Error
			$array = array(
				'status' => (int)0,
				'msg' => (string)'Llamada inválida.'
			);

			//Imprimimos el Arreglo
			$this->printJSON($array);
			$output = TRUE;
		}
	}
	
}