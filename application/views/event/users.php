
		<div class="container-fluid solitude">
			<div class="container">
				<div class="row">
					<div class="col s12 m12 l12">
						<div class="space20"></div>
						<div class="right">
							<a class="waves-effect waves-light btn green" href="<?php echo base_url(); ?>event/export/<?php echo $event->idevent; ?>">Exportar a Excel</a>
						</div>
						<a href="<?php echo base_url(); ?>dashboard"><i class="material-icons aluminum-text font32">chevron_left</i></a> <span class="open-sans-light font32"><?php echo $event->name; ?></span>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m6 l3">
						<center>
							<span class="open-sans-light font14">Códigos totales</span>
							<div class="white">
								<span class="open-sans-light font24"><?php echo $event->codes; ?></span>
							</div>
						</center>
					</div>
					<div class="col s12 m6 l3">
						<center>
							<span class="open-sans-light font14">Participantes</span>
							<div class="white">
								<span class="open-sans-light font24"><?php echo count($users); ?></span>
							</div>
						</center>
					</div>
					<div class="col s12 m6 l3">
						<center>
							<span class="open-sans-light font14">Códigos generados</span>
							<div class="white">
								<span class="open-sans-light font24"><?php echo $event->codes; ?></span>
							</div>
						</center>
					</div>
					<div class="col s12 m6 l3">
						<center>
							<span class="open-sans-light font14">Códigos redimidos</span>
							<div class="white">
								<span class="open-sans-light font24"><?php echo count($users_used); ?></span>
							</div>
						</center>
					</div>
				</div>
				<div class="row">
					<div class="col s12 m12 l12">
						<center>
							<span class="open-sans-light font24">PARTICIPANTES</span>
						</center>
						<?php if (count($users) > 0) { ?>
						<table class="bordered striped responsive-table">
							<thead>
								<tr>
									<th data-field="id">ID</th>
									<th data-field="fb_id">FB ID</th>
									<th data-field="nombre">Nombre</th>
									<th data-field="email">Email</th>
									<th data-field="birthday">Fecha de Nacimiento</th>
									<th data-field="code">Código</th>
									<th data-field="redeem">Redimido</th>
          						</tr>
        					</thead>

							<tbody>
								<?php foreach ($users as $user) { ?>
								<tr>
									<td><?php echo $user->iduser; ?></td>
									<td><?php echo $user->fb_id; ?></td>
									<td><?php echo $user->name . ' ' . $user->lastname; ?></td>
									<td><?php echo $user->email; ?></td>
									<td><?php echo $user->birthday; ?></td>
									<td><?php echo $user->code; ?></td>
									<td><?=($user->redimido==1) ? 'Si' : 'No'; ?></td>
          						</tr>
          						<?php } ?>
        					</tbody>
      					</table>
						<?php } else { ?>
						<span class="open-sans-light font14">No hay participantes registrados por el momento.</span>
						<?php } ?>
      					<div class="space40"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid white">
			<div class="container">
				<div class="row">
					<div class="col s12">
						<img src="<?php echo base_url(); ?>assets/img/u16.png" class="responsive-img" />
					</div>
				</div>
			</div>
		</div>