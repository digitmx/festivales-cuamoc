
		<div class="container-fluid solitude">
			<div class="container">
				<div class="row">
					<div class="col s12 m12 l12">
						<div class="space20"></div>
						<a href="<?php echo base_url(); ?>dashboard"><i class="material-icons aluminum-text font32">chevron_left</i></a> <span class="open-sans-light font32">Nuevo Festival</span>
					</div>
				</div>
			</div>
			<div class="container white padding-1rem">
				<div class="row padding-1rem">
					<div class="col s12 m12 l12">
						<form id="formNewEvent" name="formNewEvent" method="post" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url(); ?>event/saveEvent">
							<div class="row">
								<div class="input-field col s12 m12 l6">
									<span class="open-sans-light font32 block">Datos del evento</span>
									<div class="space20"></div>
									<div class="row">
							        	<label class="open-sans-light font14 block" for="inputNombre">Nombre festival</label>
										<input placeholder="" id="inputNombre" name="inputNombre" type="text" autocomplete="off">
									</div>
									<div class="row">
										<label class="open-sans-light font14 block" for="inputCofepris">Número COFEPRIS</label>
										<input placeholder="" id="inputCofepris" name="inputCofepris" type="text" autocomplete="off">
									</div>
									<div class="row">
										<label class="open-sans-light font14 block" for="inputPremio">Premio</label>
										<textarea id="inputPremio" name="inputPremio" rows="3" style="resize: none;"></textarea>
									</div>
									<div class="row">
										<label class="open-sans-light font14 block" for="inputCodigos">Códigos Totales</label>
										<input placeholder="" id="inputCodigos" name="inputCodigos" type="number" autocomplete="off">
									</div>
									<div class="row">
										<label class="open-sans-light font14 block" for="inputMensaje">Mensaje de Códigos Agotados</label>
										<textarea id="inputMensaje" name="inputMensaje" rows="3" style="resize: none;"></textarea>
									</div>
									<div class="space20"></div>
									<span class="open-sans-light font32 block">Dinámica</span>
									<div class="space20"></div>
									<div class="row">
										<label class="open-sans-light font14 block" for="inputPaso1">PASO 1</label>
										<input placeholder="" id="inputPaso1" name="inputPaso1" type="text" autocomplete="off">
									</div>
									<div class="row">
										<label class="open-sans-light font14 block" for="inputPaso2">PASO 2</label>
										<input placeholder="" id="inputPaso2" name="inputPaso2" type="text" autocomplete="off">
									</div>
									<div class="row">
										<label class="open-sans-light font14 block" for="inputPaso3">PASO 3</label>
										<input placeholder="" id="inputPaso3" name="inputPaso3" type="text" autocomplete="off">
									</div>
									<div class="row">
										<label class="open-sans-light font14 block" for="inputInstrucciones">Instrucciones de canjeo</label>
										<textarea id="inputInstrucciones" name="inputInstrucciones" rows="3" style="resize: none;"></textarea>
									</div>
					        	</div>
					        	<div class="input-field col s12 m12 l6">
						        	<span class="open-sans-light font32 block">Imágenes del evento</span>
									<div class="space20"></div>
									<span class="open-sans-light font14 block">Logo</span>
									<img src="<?php echo base_url(); ?>assets/img/u241.png" class="responsive-img" id="fileLogo" name="fileLogo" />
									<input type="file" class="file_form" id="inputLogo" name="inputLogo" style="display: none;" />
									<span class="open-sans-light font14 block">Cartel</span>
									<img src="<?php echo base_url(); ?>assets/img/u241.png" class="responsive-img" id="fileCartel" name="fileCartel" />
									<input type="file" class="file_form" id="inputCartel" name="inputCartel" style="display: none;" />
									<span class="open-sans-light font14 block">Fondo</span>
									<img src="<?php echo base_url(); ?>assets/img/u241.png" class="responsive-img" id="fileFondo" name="fileFondo" />
									<input type="file" class="file_form" id="inputFondo" name="inputFondo" style="display: none;" />
					        	</div>
							</div>
							<div class="row">
								<div class="col s12">
									<div class="right">
										<a class="waves-effect waves-light btn green" id="btnNewEvent" name="btnNewEvent">Crear Evento</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid white">
			<div class="container">
				<div class="row">
					<div class="col s12">
						<img src="<?php echo base_url(); ?>assets/img/u16.png" class="responsive-img" />
					</div>
				</div>
			</div>
		</div>