
		<div class="container-fluid white">
			<div class="container">
				<div class="row">
					<div class="col s12 m8 offset-m2 l6 offset-l3">
						<div class="space40"></div>
						<center>
							<img src="<?php echo base_url(); ?>assets/img/u2.png" class="responsive-img" />
						</center>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid solitude">
			<div class="container">
				<div class="row">
					<div class="col s12 m8 offset-m2 l6 offset-l3">
						<div class="space10"></div>
						<center>
							<span class="open-sans-light font32 block">FESTIVALES</span>
							<div class="space20"></div>
							<form id="formLogin" name="formLogin" method="post" accept-charset="utf-8">
						    	<div class="row">
						        	<div class="input-field col s12">
							        	<label class="open-sans-light font14" for="inputEmail">Email</label>
										<input placeholder="" id="inputEmail" name="inputEmail" type="text">
						        	</div>
						        	<div class="input-field col s12">
							        	<label class="open-sans-light font14" for="inputPassword">Contraseña</label>
										<input placeholder="" id="inputPassword" name="inputPassword" type="password">
						        	</div>
						      	</div>
						      	<div class="row">
							      	<a class="waves-effect waves-light btn green" id="btnLogin" name="btnLogin">ENTRAR</a>
						      	</div>
							</form>
						</center>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid white">
			<div class="container">
				<div class="row">
					<div class="col s12">
						<img src="<?php echo base_url(); ?>assets/img/u16.png" class="responsive-img" />
					</div>
				</div>
			</div>
		</div>