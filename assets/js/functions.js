
	$( document ).ready(function() {

		//Read Values
		var base_url = $('#base_url').val();

		// Initialize collapse button
		$(".button-collapse").sideNav();
		// Initialize collapsible (uncomment the line below if you use the dropdown variation)
		//$('.collapsible').collapsible();
		
		//formLogin Validate
		$('#formLogin').validate({
			rules: {
				inputEmail: {
					required: true,
					email: true
				},
				inputPassword: {
					required: true
				}
			}
		});
		
		//btnLogin Click
		$('#btnLogin').on('click', function(e) {
			e.preventDefault();
			
			//Check Form
			if ($('#formLogin').valid())
			{
				//Read Values
				var email = $('#inputEmail').val();
				var password = $('#inputPassword').val();
				var param = '{"msg": "loginAdmin","fields": {"email": "' + email + '", "password": "' + password + '"}}';

				//Disable Button
				$('#btnLogin').prop( 'disabled', true );
				$('#btnLogin').html('PROCESANDO...');

				//API Call
				$.post(base_url + 'api', { param: param }).done(function( data ) {
					//Check Status Call
					if (data.status == 1)
					{
						//Reload
						location.reload();
					}
					else
					{
						//Show Error
						Materialize.toast(data.msg, 4000);

						//Enable Button
						$('#btnLogin').prop( 'disabled', false );
		                $('#btnLogin').html('ENTRAR');
					}
				});
			}
			
			return false;
		});
		
		//formNewEvent Validate
		$('#formNewEvent').validate({
			rules: {
				inputNombre: {
					required: true
				},
				inputCofepris: {
					required: true
				},
				inputPremio: {
					required: true
				},
				inputCodigos: {
					required: true,
					digits: true
				},
				inputMensaje: {
					required: true
				},
				inputPaso1: {
					required: true
				},
				inputPaso2: {
					required: true
				},
				inputPaso3: {
					required: true
				},
				inputInstrucciones: {
					required: true
				},
				inputLogo: {
					required: true,
					extension: "jpg|bmp|gif|jpeg"
				},
				inputCartel: {
					required: true,
					extension: "jpg|bmp|gif|jpeg"
				},
				inputFondo: {
					required: true,
					extension: "jpg|bmp|gif|jpeg"
				}
			}
		})
		
		//btnNewEvent Click
		$('#btnNewEvent').on('click', function(e) {
			e.preventDefault();
			
			//Check Form
			if ($('#formNewEvent').valid())
			{
				//Bandera
				var bandera = false;
				
				//Leemos las Imagenes
				var logo = $('#inputLogo').val();
				var cartel = $('#inputCartel').val();
				var fondo = $('#inputFondo').val();
				if (logo && cartel && fondo)
				{
					//Procesamos el Evento
					$('#formNewEvent').submit();
				}
				else
				{
					//Show Message
					Materialize.toast("Las imágenes son un campo obligatorio. Da click en la imagen para editar.", 4000);
				}
			}
			
			return false;
		});
		
		//fileLogo Click
		$('#fileLogo').on('click', function(e) {
			e.preventDefault();

			$('#inputLogo').trigger('click');

			return false;
		});
		
		//fileCartel Click
		$('#fileCartel').on('click', function(e) {
			e.preventDefault();

			$('#inputCartel').trigger('click');

			return false;
		});
		
		//fileFondo Click
		$('#fileFondo').on('click', function(e) {
			e.preventDefault();

			$('#inputFondo').trigger('click');

			return false;
		});
		
		//#inputLogo Change
		$('#inputLogo').on('change', function(e) {
			e.preventDefault();

			//Read Values
			var file = $(this).val();
			var extension = file.substr( (file.lastIndexOf('.') +1) );
			
			//Check Extension
			if (extension.toLowerCase() == 'jpg' || extension.toLowerCase() == 'png' || extension.toLowerCase() == 'jpeg')
			{
				readURL(this,'#fileLogo');
			}
			else
			{
				//Show Error
				Materialize.toast("No es un formato válido de imagen.", 4000);
			}

			return false;
		});
		
		//#inputCartel Change
		$('#inputCartel').on('change', function(e) {
			e.preventDefault();

			//Read Values
			var file = $(this).val();
			var extension = file.substr( (file.lastIndexOf('.') +1) );
			
			//Check Extension
			if (extension.toLowerCase() == 'jpg' || extension.toLowerCase() == 'png' || extension.toLowerCase() == 'jpeg')
			{
				readURL(this,'#fileCartel');
			}
			else
			{
				//Show Error
				Materialize.toast("No es un formato válido de imagen.", 4000);
			}

			return false;
		});
		
		//#btnDelete Click
		$('#btnDeleteEvent').on('click', function(e) {
			
			//Confirm Delete
			var r = confirm("¿Estás seguro de eliminar el evento?");
			if (r != true) {
			   e.preventDefault();
			   return false;
			}			
			
		});
		
		//#inputFondo Change
		$('#inputFondo').on('change', function(e) {
			e.preventDefault();

			//Read Values
			var file = $(this).val();
			var extension = file.substr( (file.lastIndexOf('.') +1) );
			
			//Check Extension
			if (extension.toLowerCase() == 'jpg' || extension.toLowerCase() == 'png' || extension.toLowerCase() == 'jpeg')
			{
				readURL(this,'#fileFondo');
			}
			else
			{
				//Show Error
				Materialize.toast("No es un formato válido de imagen.", 4000);
			}

			return false;
		});
		
		//PREVIEW IMAGES
		function readURL(input,preview) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        reader.onload = function (e) {
		            $(preview).attr('src', e.target.result);
		        }
		        reader.readAsDataURL(input.files[0]);
		    }
		}
		
		//formEditEvent Validate
		$('#formEditEvent').validate({
			rules: {
				inputNombre: {
					required: true
				},
				inputCofepris: {
					required: true
				},
				inputPremio: {
					required: true
				},
				inputCodigos: {
					required: true,
					digits: true
				},
				inputMensaje: {
					required: true
				},
				inputPaso1: {
					required: true
				},
				inputPaso2: {
					required: true
				},
				inputPaso3: {
					required: true
				},
				inputInstrucciones: {
					required: true
				},
				inputLogo: {
					required: true,
					extension: "jpg|bmp|gif|jpeg"
				},
				inputCartel: {
					required: true,
					extension: "jpg|bmp|gif|jpeg"
				},
				inputFondo: {
					required: true,
					extension: "jpg|bmp|gif|jpeg"
				}
			}
		})
		
		//btnUpdateEvent Click
		$('#btnUpdateEvent').on('click', function(e) {
			e.preventDefault();
			
			//Check Form
			if ($('#formEditEvent').valid())
			{
				//Procesamos el Evento
				$('#formEditEvent').submit();
			}
			
			return false;
		});
		
	});