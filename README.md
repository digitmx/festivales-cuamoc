## Synopsis

Backend Festivales Cuauhtémoc

## Code Example

La página web esta en:

* [Festivales Cuauhtémoc](http://www.fooprojects.com/clients/festivales-cuamoc/)

## Motivation

Backend para administrar Eventos y Usuarios para los Festivales Cuauhtémoc.

## Installation

El proyecto esta basado en CodeIgniter. Hay que configurar la base de datos para que se conecte correctamente, en la carpeta "der" viene el diagrama entidad relación y en la carpeta "sql" viene el script de base de datos.

## Contributors

* Emiliano Hernández ([@milioh](https://twitter.com/milioh))

## License

Copyright 2016 - FOO STUDIO

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.